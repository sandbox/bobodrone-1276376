<?php
/**
 * @file
 * ns_ch_int.features.inc
 */

/**
 * Implementation of hook_ctools_plugin_api().
 */
function ns_ch_int_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implementation of hook_node_info().
 */
function ns_ch_int_node_info() {
  $items = array(
    'ns_ch_int_group' => array(
      'name' => t('Group'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Group name'),
      'help' => '',
    ),
  );
  return $items;
}
